<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Registration Page (v2)</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('adminLTE/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('adminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('adminLTE/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition register-page">
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-4">
                  <div class="card card-outline card-primary">
                    <div class="card-header text-center">
                      <a href="../../index2.html" class="h1">{{ __('Register') }}</a>
                    </div>
                    <div class="card-body">
                      <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" name="name" placeholder="Full name">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-user"></span>
                            </div>
                          </div>
                          @error('name')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div class="input-group mb-3">
                          <input type="email" class="form-control" name="email" placeholder="Email">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-envelope"></span>
                            </div>
                          </div>
                          @error('email')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                        <div class="input-group mb-3">
                          <input type="password" class="form-control" name="password" placeholder="Password">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-lock"></span>
                            </div>
                          </div>
                          @error('password')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div class="input-group mb-3">
                          <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-lock"></span>
                            </div>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <input type="number" class="form-control" name="umur" placeholder="Enter your Age">
                          @error('umur')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div class="input-group mb-3">
                          <textarea name="bio" class="form-control" placeholder="Enter your Bio"></textarea>
                          @error('bio')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div class="input-group mb-3">
                          <textarea name="alamat" class="form-control" placeholder="Enter your Address"></textarea>
                          @error('alamat')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
  
                        <div class="row">
                          <div class="col">
                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                            <a href="/login" class="text-center">I already have a membership</a>
                          </div>
                        </div>
                      </form>  
                  </div>
          </div>
      </div>
  </div>

<!-- jQuery -->
<script src="{{asset("adminLTE/plugins/jquery/jquery.min.js")}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminLTE/dist/js/adminlte.min.js')}}"></script>
</body>
</html>
