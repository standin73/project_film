@extends('layouts.master')

@section('judul')
    Detail film {{$film->judul}}
@endsection

@section('content')
    <img src="{{asset('gambar/'.$film->poster)}}" alt="">
    <h6>Tahun {{$film->tahun}}</h6><br>
    <p>{{$film->ringkasan}}</p>
    <p>Tanggal dibuat : {{$film->created_at}}</p>


    @forelse ($film->komentar as $value)
    <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">{{$value->user->name}}</h5>
          <p class="card-text">{{$value->komentar}}</p>
        </div>
    </div>
    @empty
        <h2>Belum ada komentar</h2>
    @endforelse

    <form action="/komentar" method="POST">
        @csrf 
          <div class="form-group">
              <label>Komentar</label>
              <input type="hidden" value="{{$film->id}}"  name="film_id">
              <textarea name="komentar" class="form-control" id="" cols="30" rows="10"></textarea>
          </div>
          @error('komentar')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <button type="submit" class="btn btn-primary">Komentar</button>
      </form>
@endsection