@extends('layouts.master')

@section('judul')
    List Film
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary btn-sm mb-3">Tambah Film</a>    
@endauth
  
    <div class="row">
        @forelse ($film as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('gambar/' . $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <span class="badge badge-info">{{$item->genre->nama}}</span><br>
                  <h2 class="card-title">{{$item->judul}}</h2><br>
                    <h6>Tahun : {{$item->tahun}}</h6>
                  <p class="card-text">{{Str::limit($item->ringkasan, 150)}}</p>
                  @auth
                    <form action="/film/{{$item->id}}" method="POST">
                      @csrf
                      @method("DELETE")
                      <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                      <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>    
                  @endauth
                  @guest
                  <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                  @endguest
                    
                </div>
            </div>
        </div>
        @empty
            <h2>Data Film Masih Kosong</h2>
        @endforelse
        
    </div>
@endsection