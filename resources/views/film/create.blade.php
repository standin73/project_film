@extends('layouts.master')

@section('judul')
    Tambah Film
@endsection

@push('style')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
});  
</script>  
@endpush

@push('script')
<script src="https://cdn.tiny.cloud/1/iyfcbzh05iw272ko13oj1p63nakinjh06w3r8pec1kh7872c/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush


@section('content')
<form action="/film" method="POST" enctype="multipart/form-data">
  @csrf 
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" class="form-control" >
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class="form-control"></textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>tahun</label>
        <input type="text" name="tahun" class="form-control" >
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control" >
    </div>
    @error('poster')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Genre</label>
      <select name="genre_id" class="js-example-basic-single" style= "width : 100%, height : 20px">
          <option value="">---- Pilih Genre ----</option>
          @foreach ($genre as $item)
              <option value="{{$item->id}}">{{$item->nama}}</option>
          @endforeach
      </select>
    </div>
    @error('genre')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection