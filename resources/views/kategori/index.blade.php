@extends('layouts.master')

@section('judul')
    Genre Dari Setiap Film
@endsection

@section('content')
<a href="/genre/create" class="btn btn-warning mb-3">Tambah Genre</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Judul Film</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key=>$value)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>
              <ul>
                @foreach ($value->film as $item)
                    <li>{{$item->judul}}</li>
                @endforeach
              </ul>
            </td>
            <td>
                <a href="/genre/{{$value->id}}" class="btn btn-info">Show</a>
                <a href="/genre/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                {{-- <a href="/genre/{{$value->id}}" class="btn btn-danger">Delete</a> --}}
                <form action="/genre/{{$value->id}}" method="post">
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
          </tr>
      @empty
          <tr clspan='3'>
            <h3>Data Masih Kosong</h3>
          </tr>
      @endforelse
    </tbody>
  </table>    
@endsection