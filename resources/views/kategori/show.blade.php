@extends('layouts.master')

@section('judul')
    Details genre {{$genre->nama}}
@endsection

@section('content')
    <div class="row">
        @forelse ($genre->film as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('gambar/' . $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h2 class="card-title">{{$item->judul}}</h2><br>
                    <h6>Tahun : {{$item->tahun}}</h6>
                  <p class="card-text">{{Str::limit($item->ringkasan, 150)}}</p>
                  @guest
                  <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                  @endguest
                    
                </div>
            </div>
        </div>
        @empty
            <h2>Data Film Masih Kosong</h2>
        @endforelse
        
    </div>
@endsection