@extends('layouts.master')

@section('judul')
    Daftar pemain film
@endsection

@section('content')
<a class="btn btn-primary mb-2" href="/cast/create" role="button">Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key=>$value)
        <tr>
          <th>{{$key + 1}}</th>
          <th>{{$value->nama}}</th>
          <th>{{$value->umur}}</th>
          <th>{{Str::limit($value->bio, 30)}}</th>
          <th>
            <a class="btn btn-success" href="/cast/{{$value->id}}/detail" role="button">Detail</a>
            <a class="btn btn-warning" href="/cast/{{$value->id}}/edit" role="button">Update</a>
            <a class="btn btn-danger" href="/cast/{{$value->id}}/delete" role="button">Delete</a>
          </th>
          
        </tr>
      @empty
          <h2>Data Masih Kosong</h2>
      @endforelse  
    </tbody>
  </table>
@endsection