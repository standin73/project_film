@extends('layouts.master')

@section('judul')
    Tambah daftar caster
@endsection

@section('content')
<form action="/cast/{{$cast->id}}/update" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" aria-describedby="emailHelp">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" aria-describedby="emailHelp">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  
@endsection