@extends('layouts.master')

@section('judul')
    Edit Profile
@endsection


@section('content')
<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method("PUT")
      <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" name="name" value="{{$profile->user->name}}" disabled>
      </div>
      <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control" name="email" value="{{$profile->user->email}}" disabled>
      </div>
      <div class="form-group">
        <label>Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$profile->umur}}">
      </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Bio</label>
        <textarea class="form-control" name="bio">{{$profile->bio}}</textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Alamat</label>
        <textarea class="form-control" name="alamat">{{$profile->alamat}}</textarea>
      </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection