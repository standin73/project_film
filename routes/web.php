<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});



Route::group(['middleware' => ['auth']], function () {
    // genre
    Route::get('/genre/create', 'KategoriController@create');
    Route::post('/genre', 'KategoriController@save');
    Route::get('/genre' ,'KategoriController@index');
    Route::get('/genre/{genre_id}' ,'KategoriController@show');
    Route::get('/genre/{genre_id}/edit' , 'KategoriController@edit');
    Route::put('/genre/{genre_id}', 'KategoriController@update');
    Route::delete('/genre/{genre_id}', 'KategoriController@delete'); 
    //Genre
    Route::get('/cast' , 'CastController@index');
    Route::get('/cast/create' , 'CastController@create');
    Route::post('/cast/add' , 'CastController@add');
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');
    Route::put('/cast/{cast_id}/update', 'CastController@update');
    Route::get('/cast/{cast_id}/detail', 'CastController@detail');
    Route::get('/cast/{cast_id}/delete', 'CastController@delete');

    //Updatae profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('komentar', 'KomentarController')->only(['store']);
});


// Film
Route::resource('film', 'FilmController'); 
Auth::routes();

