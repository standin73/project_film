<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = 'Film';
    protected $fillable = ['judul','ringkasan','tahun','poster','genre_id'];

    public function genre()
    {
        return $this->belongsTo('App\genre');
    }

    public function komentar()
    {
        return $this->hasMany('App\komentar');
    }
}
