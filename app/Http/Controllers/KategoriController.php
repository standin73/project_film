<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\genre;
use RealRashid\SweetAlert\Facades\Alert;

class KategoriController extends Controller
{
    public function create(){
        return view('kategori.create');
    }
    public function save(Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        DB::table('genre')->insert([
            'nama' => $request['nama'],
        ]);

        Alert::success('Mantap', 'Berhasil Tambah Genre');
        return redirect('/genre');
    }

    public function index(){
        $genre = genre::all();
        return view('kategori.index', compact('genre'));
    }

    public function show($id){
        $genre = genre::find($id);
        return view('kategori.show', compact('genre'));
    }

    public function edit($id){
        $genre = DB::table('genre')->where('id', $id)->first();
        return view('kategori.edit', compact('genre'));
    }
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required'
        ]);

        $query = DB::table('genre')
              ->where('id', $id)
              ->update([
            'nama' => $request['nama']
        ]);
        Alert::info('Updated', 'Success Update');
        return redirect('/genre');
    }

    public function delete($id){
        $query = DB::table('genre')->where('id', $id)->delete(); 
        return redirect('/genre');
    }
    
}
