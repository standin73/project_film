<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\komentar;

class KomentarController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'komentar' => 'required'
        ]);

        $komentar = new komentar;

        $komentar->komentar = $request->komentar;
        $komentar->user_id = Auth::id();
        $komentar->film_id = $request->film_id;

        $komentar->save();

        return redirect()->back();


    }
}
