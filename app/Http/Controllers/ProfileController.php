<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $profile = profile::where('id', Auth::id())->first();
        return view('profile.index', compact('profile'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        $profile = profile::find($id);

        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];
        $profile->alamat = $request['alamat'];

        $profile->save();

        return redirect('/profile');
    }
}
